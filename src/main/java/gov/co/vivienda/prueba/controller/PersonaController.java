package gov.co.vivienda.prueba.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import gov.co.vivienda.prueba.dto.PaisRequest;
import gov.co.vivienda.prueba.dto.PersonaRequest;
import gov.co.vivienda.prueba.dto.TipoDocumentoRequest;
import gov.co.vivienda.prueba.entity.Pais;
import gov.co.vivienda.prueba.entity.Persona;
import gov.co.vivienda.prueba.entity.TipoDocumento;
import gov.co.vivienda.prueba.repository.PaisRepository;
import gov.co.vivienda.prueba.repository.PersonaRepository;
import gov.co.vivienda.prueba.repository.TipoDocumentoRepository;

@RestController
@RequestMapping("/persona")
public class PersonaController {

	@Autowired
	private PersonaRepository personaRepository;
	@Autowired
	private TipoDocumentoRepository tipoDocumentoRepository;
	@Autowired
	private PaisRepository paisRepository;
	
	@PostMapping("/createPersona")
	public Persona createPersona(@RequestBody PersonaRequest request) {
		return personaRepository.save(request.getPersona());
	}
	
	@PostMapping("/updatePersona")
	public Persona updatePersona(@RequestBody PersonaRequest request) {
		Persona personaUpdate = personaRepository.findById(request.getPersona().getId()).get();
		personaUpdate = request.getPersona();
		return personaRepository.save(personaUpdate);
	}
	
	@DeleteMapping("/deletePersona")
	public void deletePersona(@RequestBody PersonaRequest request) {
		personaRepository.delete(request.getPersona());
	}
	
	@GetMapping("/getPersonaId")
	public Persona findPersonaById(@RequestParam Integer id) {
		return personaRepository.findById(id).get();
	}
	
	@GetMapping("/getPersonas")
	public List<Persona> findAllPersonas(){
		return personaRepository.findAll();
	}
	
	@PostMapping("/createTipoDocumento")
	public TipoDocumento createTipoDocumento(@RequestBody TipoDocumentoRequest request) {
		return tipoDocumentoRepository.save(request.getTipoDocumento());
	}
	
	@PostMapping("/updateTipoDocumento")
	public TipoDocumento updateTipoDocumento(@RequestBody TipoDocumentoRequest request) {
		TipoDocumento tipoDocumentoUpdate = tipoDocumentoRepository.findById(request.getTipoDocumento().getId()).get();
		return tipoDocumentoRepository.save(tipoDocumentoUpdate);
	}
	
	@DeleteMapping("/deleteTipoDocumento")
	public void deleteTipoDocumento(@RequestBody TipoDocumentoRequest request) {
		tipoDocumentoRepository.delete(request.getTipoDocumento());
	}
	
	@GetMapping("/getTiposDocumento")
	public List<TipoDocumento> findAllTiposDocumento(){
		return tipoDocumentoRepository.findAll();
	}
	
	@PostMapping("/createPais")
	public Pais createPais(@RequestBody PaisRequest request) {
		return paisRepository.save(request.getPais());
	}
	@PostMapping("/updatePais")
	public Pais updatePais(@RequestBody PaisRequest request) {
		Pais paisUpdate = paisRepository.findById(request.getPais().getId()).get();
		return paisRepository.save(paisUpdate);
	}
	
	@DeleteMapping("/deletePais")
	public void deletePais(@RequestBody PaisRequest request) {
		paisRepository.delete(request.getPais());
	}
	
	@DeleteMapping("/deletePersonaPais")
	public void deletePersonaPais(@RequestBody PaisRequest request) {
		System.out.println("Este método es nuevo y elimina todas las personas de un país");
	}
	
	@PostMapping
	public Pais linkPais(@RequestBody PaisRequest request) {
		return null;
	}
	
	@GetMapping("/getPaises")
	public List<Pais> findAllPaises(){
		return paisRepository.findAll();
	}
}
