package gov.co.vivienda.prueba.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import gov.co.vivienda.prueba.entity.TipoDocumento;

public interface TipoDocumentoRepository extends JpaRepository<TipoDocumento, Integer>{

}
