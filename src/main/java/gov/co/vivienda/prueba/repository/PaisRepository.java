package gov.co.vivienda.prueba.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import gov.co.vivienda.prueba.entity.Pais;


public interface PaisRepository extends JpaRepository<Pais, Integer> {

}
