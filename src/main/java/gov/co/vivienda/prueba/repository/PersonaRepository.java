package gov.co.vivienda.prueba.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import gov.co.vivienda.prueba.entity.Persona;

public interface PersonaRepository extends JpaRepository<Persona, Integer>{ 

}
